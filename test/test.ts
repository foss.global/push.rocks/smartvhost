import { expect, tap } from '@pushrocks/tapbundle';
import * as smartvhost from '../ts/index';
import { SmartVHost } from '../ts/index';

import * as path from 'path';

let testSmartVHost: smartvhost.SmartVHost;

tap.test('should create a valid instance of smartvhost', async () => {
  testSmartVHost = new smartvhost.SmartVHost();
  expect(testSmartVHost).to.be.instanceOf(smartvhost.SmartVHost);
});

tap.test('should start smartvhost', async () => {
  await testSmartVHost.start();
  
});

tap.test('should accept a config array', async () => {
  testSmartVHost.setVHostConfigs([{
    hostName: 'example.com',
    privateKey: '',
    publicKey: '',
    type: 'folder',
    target: path.join(__dirname, 'testservedir')
  }]);
});

tap.test('should stop smartvhost', async () => {
  await testSmartVHost.stop();
});

tap.start();
