export type TVHostConfigType = 'folder' | 'ipAndPort' | 'localPort' | 'domain';

export interface IVHostConfig {
  hostName: string;
  type: TVHostConfigType;
  target:  string;
  privateKey: string;
  publicKey: string;
}