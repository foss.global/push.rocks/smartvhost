import * as plugins from './smartvhost.plugins';
import * as interfaces from './interfaces';

export class SmartVHost {
  public currentConfig: interfaces.IVHostConfig[];
  public smartproxy: plugins.smartproxy.SmartProxy;
  public smartexpress: plugins.smartexpress.Server;

  constructor() {
    this.smartproxy = new plugins.smartproxy.SmartProxy({
      port: 3000
    });
    this.smartexpress = new plugins.smartexpress.Server({
      cors: true,
      forceSsl: false,
      port: 3001,
      robots: 'standard'
    });
  }

  public async start() {
    await this.smartproxy.start();
    await this.smartexpress.start();
  }

  public async stop() {
    await this.smartproxy.stop();
    await this.smartexpress.stop();
  }

  public setVHostConfigs(configArray: interfaces.IVHostConfig[]) {
    this.currentConfig = configArray;    

    // lets route the traffic using smartproxy
    const reverseConfigs: plugins.tsclass.network.IReverseProxyConfig[] = [];
    for (const vHostConfig of this.currentConfig) {
      switch(vHostConfig.type) {
        case 'folder':
          reverseConfigs.push({
            destinationIp: '0.0.0.0',
            destinationPort: '3001',
            hostName: vHostConfig.hostName,
            privateKey: vHostConfig.privateKey,
            publicKey: vHostConfig.publicKey,
          });
          break;
        case 'ipAndPort':
          reverseConfigs.push({
            destinationIp: vHostConfig.target.split(':')[0],
            destinationPort: vHostConfig.target.split(':')[1],
            hostName: vHostConfig.hostName,
            privateKey: vHostConfig.privateKey,
            publicKey: vHostConfig.publicKey
          });
          break;
        case 'localPort':
          reverseConfigs.push({
            destinationIp: '0.0.0.0',
            destinationPort: vHostConfig.target,
            hostName: vHostConfig.hostName,
            privateKey: vHostConfig.privateKey,
            publicKey: vHostConfig.publicKey,
          });
          break;
        case 'domain':
          break;
        default: 
          throw new Error(`unknown config type ${vHostConfig.type}`);
      }
    }
    this.smartproxy.updateReverseConfigs(reverseConfigs);
  }
}
